import React from 'react';
import PropTypes from 'prop-types';

function FurnitureForm({ onTypeAttrSet, typeAttr }) {
  const newTypeAttr = typeAttr;
  return (
    <div className="furniture-container">
      <label htmlFor="height" className="product-form__label">
        <span>Height (CM)</span>
        <input
          type="number"
          name="height"
          id="height"
          onChange={(e) => {
            newTypeAttr.height = {
              valType: 'number',
              value: e.target.value,
            };
            onTypeAttrSet(newTypeAttr);
          }}
          placeholder="Insert height"
          required
        />
      </label>
      <label htmlFor="width" className="product-form__label">
        <span>Width (CM)</span>
        <input
          type="number"
          name="width"
          id="width"
          onChange={(e) => {
            newTypeAttr.width = {
              valType: 'number',
              value: e.target.value,
            };
            onTypeAttrSet(newTypeAttr);
          }}
          placeholder="Insert width"
          required
        />
      </label>
      <label htmlFor="length" className="product-form__label">
        <span>Length (CM)</span>
        <input
          type="number"
          name="length"
          id="length"
          onChange={(e) => {
            newTypeAttr.length = {
              valType: 'number',
              value: e.target.value,
            };
            onTypeAttrSet(newTypeAttr);
          }}
          placeholder="Insert length"
          required
        />
      </label>
      <p className="product-form__description">
        Please, provide dimensions in
        <span className="product-form__description-unit"> centimeters</span>
      </p>
    </div>
  );
}

FurnitureForm.propTypes = {
  onTypeAttrSet: PropTypes.func.isRequired,
  typeAttr: PropTypes.shape({
    height: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
    width: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
    length: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
  }).isRequired,
};

export default FurnitureForm;
