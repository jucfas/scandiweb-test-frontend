import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

function HeaderAddProduct({ onSubmitForm }) {
  return (
    <header className="page-header">
      <h1 className="page-title">Product Add</h1>
      <div className="buttons">
        <button type="button" form="product_form" className="btn btn--primary" onClick={onSubmitForm}>Save</button>
        <Link to="/" className="btn btn--secondary">Cancel</Link>
      </div>
    </header>
  );
}

HeaderAddProduct.propTypes = {
  onSubmitForm: PropTypes.func.isRequired,
};

export default HeaderAddProduct;
