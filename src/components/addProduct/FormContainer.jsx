import React from 'react';
import PropTypes from 'prop-types';
import BookForm from './BookForm';
import DVDForm from './DVDForm';
import FurnitureForm from './FurnitureForm';
import '../css/add-product.css';

function FormContainer({
  generalAttr,
  onChangeGeneralAttr,
  typeAttr,
  onTypeSelect,
  onTypeAttrSet,
}) {
  return (
    <form id="product_form" className="product-form">
      <label className="product-form__label" htmlFor="sku">
        <span>SKU</span>
        <input
          type="text"
          name="sku"
          id="sku"
          className="product-form__label-input"
          onChange={(e) => onChangeGeneralAttr(e, 'sku', 'text')}
          placeholder="Insert unique SKU"
          required
        />
      </label>
      <label className="product-form__label" htmlFor="name">
        <span>Name</span>
        <input
          type="text"
          name="name"
          id="name"
          className="product-form__label-input"
          onChange={(e) => onChangeGeneralAttr(e, 'name', 'text')}
          placeholder="Insert name"
          required
        />
      </label>
      <label className="product-form__label" htmlFor="price">
        <span>Price ($)</span>
        <input
          type="number"
          name="price"
          id="price"
          className="product-form__label-input"
          step="0.01"
          onChange={(e) => onChangeGeneralAttr(e, 'price', 'number')}
          placeholder="Insert price"
          required
        />
      </label>

      <label htmlFor="product_type">Type Switcher</label>
      <select
        name="product_type"
        id="productType"
        className="product-form__select"
        onChange={(e) => onTypeSelect({
          valType: 'text',
          value: e.target.value,
        })}
        required
      >
        <option value="">Type Switcher</option>
        <option value="DVD" id="DVD">
          DVD
        </option>
        <option value="furniture" id="Furniture">
          Furniture
        </option>
        <option value="book" id="Book">
          Book
        </option>
      </select>

      {generalAttr.type.value === 'DVD' && <DVDForm typeAttr={typeAttr} onTypeSelect={onTypeSelect} onTypeAttrSet={onTypeAttrSet} />}
      {generalAttr.type.value === 'furniture' && <FurnitureForm typeAttr={typeAttr} onTypeSelect={onTypeSelect} onTypeAttrSet={onTypeAttrSet} />}
      {generalAttr.type.value === 'book' && <BookForm typeAttr={typeAttr} onTypeSelect={onTypeSelect} onTypeAttrSet={onTypeAttrSet} />}
    </form>
  );
}

export default FormContainer;

FormContainer.propTypes = {
  onTypeSelect: PropTypes.func.isRequired,
  generalAttr: PropTypes.shape({
    sku: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
    name: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
    price: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
    type: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
  }).isRequired,
  onChangeGeneralAttr: PropTypes.func.isRequired,
  typeAttr: PropTypes.shape({
    size: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
    height: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
    width: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
    length: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
    weight: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
  }).isRequired,
  onTypeAttrSet: PropTypes.func.isRequired,
};
