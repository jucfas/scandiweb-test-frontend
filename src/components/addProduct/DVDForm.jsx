import React from 'react';
import PropTypes from 'prop-types';

function DVDForm({ onTypeAttrSet, typeAttr }) {
  const newTypeAttr = typeAttr;

  return (
    <div className="DVD-container">
      <label htmlFor="size" className="product-form__label">
        <span>Size (MB)</span>
        <input
          type="number"
          name="size"
          id="size"
          required
          onChange={(e) => {
            newTypeAttr.size = {
              valType: 'number',
              value: e.target.value,
            };
            onTypeAttrSet(newTypeAttr);
          }}
          placeholder="Insert size"
        />
      </label>
      <p className="product-form__description">
        Please, provide size in
        <span className="product-form__description-unit"> MB</span>
      </p>
    </div>
  );
}

DVDForm.propTypes = {
  onTypeAttrSet: PropTypes.func.isRequired,
  typeAttr: PropTypes.shape({
    size: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
  }).isRequired,
};

export default DVDForm;
