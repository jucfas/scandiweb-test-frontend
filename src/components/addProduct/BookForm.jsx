import React from 'react';
import PropTypes from 'prop-types';

function BookForm({ onTypeAttrSet, typeAttr }) {
  const newTypeAttr = typeAttr;
  return (
    <div className="book-container">
      <label htmlFor="weight" className="product-form__label">
        <span>Weight (KG)</span>
        <input
          type="number"
          name="weight"
          id="weight"
          onChange={(e) => {
            newTypeAttr.weight = {
              valType: 'number',
              value: e.target.value,
            };
            onTypeAttrSet(newTypeAttr);
          }}
          placeholder="Insert weight"
          required
        />
      </label>
      <p className="product-form__description">
        Please, provide weight in
        <span className="product-form__description-unit"> kilograms</span>
      </p>
    </div>
  );
}

BookForm.propTypes = {
  onTypeAttrSet: PropTypes.func.isRequired,
  typeAttr: PropTypes.shape({
    weight: PropTypes.shape({
      valType: PropTypes.string,
      value: PropTypes.string,
    }),
  }).isRequired,
};

export default BookForm;
