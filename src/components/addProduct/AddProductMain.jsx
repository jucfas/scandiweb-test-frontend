import { React, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import HeaderAddProduct from './HeaderAddProduct';
import FormContainer from './FormContainer';
import Modal from '../Modal';
import typesProps from '../utils/typesProps';
import setProduct from '../utils/setProduct';
import validateInput from '../utils/validateInput';

function AddProductMain() {
  const [generalAttr, setGeneralAttr] = useState({
    sku: {
      valType: 'text',
      value: '',
    },
    name: {
      valType: 'text',
      value: '',
    },
    price: {
      valType: 'number',
      value: '',
    },
    type: {
      valType: 'text',
      value: '',
    },
  });
  const [typeAttr, setTypeAttr] = useState({});
  const [modalSettings, setModalSettings] = useState({
    openModal: false,
    message: null,
  });
  const onTypeSelect = (newTypeVals) => {
    setTypeAttr(typesProps[newTypeVals.value]);
    setGeneralAttr({ ...generalAttr, type: newTypeVals });
  };

  const navigate = useNavigate();

  const onChangeGeneralAttr = (e, property, attrValType) => {
    const newObj = generalAttr;
    newObj[property] = {
      value: e.target.value,
      valType: attrValType,
    };
    setGeneralAttr(newObj);
  };

  const onTypeAttrSet = (attributes) => {
    setTypeAttr(attributes);
  };

  const onSubmitForm = async () => {
    const productObj = { ...generalAttr, ...typeAttr };
    const newProductObj = productObj;

    if (validateInput(productObj)) {
      // Build new object for POST
      Object.keys(productObj).forEach((key) => {
        newProductObj[key] = productObj[key].value;
      });
      await setProduct(newProductObj);
      navigate('/');
    } else {
      setModalSettings({
        openModal: true,
        message: 'Not all the required data has been provided, or there are invalid characters. Please try again.',
      });
    }
  };

  const onCloseModal = () => {
    setModalSettings({
      openModal: false,
      message: null,
    });
  };

  return (
    <>
      {modalSettings.openModal && (
        <Modal message={modalSettings.message} onCloseModal={onCloseModal} />
      )}
      <HeaderAddProduct onSubmitForm={onSubmitForm} />
      <FormContainer
        generalAttr={generalAttr}
        onChangeGeneralAttr={onChangeGeneralAttr}
        typeAttr={typeAttr}
        onTypeSelect={onTypeSelect}
        onTypeAttrSet={onTypeAttrSet}
      />
    </>
  );
}

export default AddProductMain;
