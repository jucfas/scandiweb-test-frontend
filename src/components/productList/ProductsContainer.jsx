import React from 'react';
import PropTypes, { func } from 'prop-types';
import '../css/product-list.css';
import { v4 as uuidv4 } from 'uuid';
import ProductGrid from './ProductGrid';

function ProductsContainer({
  products, onSelectProduct, productsToDelete,
}) {
  return (
    <main className="product-list">
      {products.map((product) => (
        <ProductGrid
          key={uuidv4()}
          productData={product}
          onSelectProduct={onSelectProduct}
          productsToDelete={productsToDelete}
        />
      ))}
    </main>
  );
}

ProductsContainer.propTypes = {
  products: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number,
      sku: PropTypes.string,
      name: PropTypes.string,
      price: PropTypes.number,
      dimensions: PropTypes.string,
      height: PropTypes.number,
      weight: PropTypes.number,
      length: PropTypes.number,
      width: PropTypes.number,
      attribute: PropTypes.string,
    }),
  ).isRequired,
  onSelectProduct: func.isRequired,
  productsToDelete: PropTypes.arrayOf(PropTypes.number).isRequired,
};

export default ProductsContainer;
