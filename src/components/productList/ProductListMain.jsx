import React, { useEffect, useState } from 'react';
import HeaderProductList from './HeaderProductList';
import '../css/product-list.css';
import ProductsContainer from './ProductsContainer';

function ProductListMain() {
  const [isLoaded, setLoaded] = useState(false);
  const [products, setProducts] = useState(null);
  const [productsToDelete, setProductsDelete] = useState([]);

  useEffect(async () => {
    const request = await fetch(
      process.env.REACT_APP_READ_PRODUCT,
    );
    const response = await request.json();
    const { data } = response;

    setProducts(data);
    setLoaded(true);
  }, []);

  const onDeleteProducts = async () => {
    // Call delete API
    if (productsToDelete.length > 0) {
      await fetch(
        process.env.REACT_APP_DELETE_PRODUCT,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            products: productsToDelete,
          }),
        },
      );
    }

    const newProducts = products.filter((product) => !productsToDelete.includes(product.id));
    setProducts(newProducts);
    setProductsDelete([]);
  };

  const onSelectProduct = (id, remove) => {
    const newProductsArr = productsToDelete;
    if (remove) {
      const index = productsToDelete.findIndex((el) => el === id);
      newProductsArr.splice(index, 1);
    } else {
      newProductsArr.push(id);
    }
    setProductsDelete(newProductsArr);
  };

  return (
    <>
      <HeaderProductList
        productsToDelete={productsToDelete}
        onDeleteProducts={onDeleteProducts}
      />
      {isLoaded && (
        <ProductsContainer
          products={products}
          productsToDelete={productsToDelete}
          onSelectProduct={onSelectProduct}
        />
      )}
    </>
  );
}

export default ProductListMain;
