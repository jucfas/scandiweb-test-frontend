import React, { useState } from 'react';
import PropTypes, { func } from 'prop-types';

function ProductGrid({ productData, onSelectProduct }) {
  const { id } = productData;
  const [isChecked, setChecked] = useState(false);

  return (
    <article className="product-grid" data-id={productData.id}>
      <input
        type="checkbox"
        name="delete"
        id="delete"
        className="delete-checkbox"
        onChange={() => {
          if (isChecked) {
            setChecked(false);
            onSelectProduct(id, true);
          } else {
            setChecked(true);
            onSelectProduct(id, false);
          }
        }}
      />
      <div className="product-grid__info">
        <p className="product-grid__info-sku">{productData.sku}</p>
        <p className="product-grid__info-name">{productData.name}</p>
        <p className="product-grid__info-price">{`${productData.price}$`}</p>
        <p className="product-grid__info-attribute">{`${productData.attribute}: ${productData[productData.attribute]} ${productData.unit}`}</p>
      </div>
    </article>
  );
}

ProductGrid.propTypes = {
  productData: PropTypes.shape({
    id: PropTypes.number,
    sku: PropTypes.string,
    name: PropTypes.string,
    price: PropTypes.number,
    dimensions: PropTypes.string,
    height: PropTypes.number,
    weight: PropTypes.number,
    length: PropTypes.number,
    width: PropTypes.number,
    attribute: PropTypes.string,
    unit: PropTypes.string,
  }).isRequired,
  onSelectProduct: func.isRequired,
};

export default ProductGrid;
