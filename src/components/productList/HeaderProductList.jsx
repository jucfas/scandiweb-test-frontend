import { React } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

function HeaderProductList({ onDeleteProducts }) {
  return (
    <header className="page-header">
      <h1 className="page-title">Product List</h1>
      <div className="buttons">
        <Link to="/add-product" className="btn btn--primary">ADD</Link>
        <button type="button" id="delete-product-btn" className="btn btn--secondary" onClick={onDeleteProducts}>MASS DELETE</button>
      </div>
    </header>
  );
}

HeaderProductList.propTypes = {
  onDeleteProducts: PropTypes.func.isRequired,
};

export default HeaderProductList;
