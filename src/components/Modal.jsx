import React from 'react';
import PropTypes from 'prop-types';
import './css/modal.css';

function Modal({ message, onCloseModal }) {
  return (
    <div className="modal-container">
      <div className="modal">
        <p className="modal__message">{message}</p>
        <button className="btn modal__btn btn--primary" type="button" onClick={onCloseModal}>Close</button>
      </div>
    </div>
  );
}

Modal.propTypes = {
  message: PropTypes.string.isRequired,
  onCloseModal: PropTypes.func.isRequired,
};

export default Modal;
