const propPlaceholder = {
  valType: '',
  value: '',
};

const typesProps = {
  DVD: {
    size: propPlaceholder,
  },
  furniture: {
    height: propPlaceholder,
    width: propPlaceholder,
    length: propPlaceholder,
  },
  book: {
    weight: propPlaceholder,
  },
};

export default typesProps;
