// Match not accepted characters
const validateRegex = {
  number: /[a-zA-Z][ `!@#$%^&*()_+\-=[\]{};':'\\|<>/?~]/,
  text: /[`@#$%^*()+=[\]{};''<>~]/,
};

const validateInput = (productObj) => {
  let validInput = false;
  const filled = Object.values(productObj).every((prop) => 'value' in prop && prop.value !== '');

  if (filled) {
    validInput = Object.keys(productObj).every((key) => {
      const prop = productObj[key];
      return validateRegex[prop.valType].test(prop.value) === false;
    });
  }

  return validInput;
};

export default validateInput;
