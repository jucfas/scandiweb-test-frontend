const setProduct = async (data) => {
  await fetch(process.env.REACT_APP_CREATE_PRODUCT, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  });
};

export default setProduct;
