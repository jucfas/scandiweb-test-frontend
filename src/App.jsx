import React from 'react';
import { Routes, Route } from 'react-router-dom';
import './components/css/reset.css';
import ProductListMain from './components/productList/ProductListMain';
import AddProductMain from './components/addProduct/AddProductMain';
import Footer from './components/Footer';

function App() {
  return (
    <>
      <Routes>
        <Route path="/" element={<ProductListMain />} />
        <Route path="/add-product" element={<AddProductMain />} />
      </Routes>
      <Footer />
    </>
  );
}

export default App;
